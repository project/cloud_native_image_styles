<?php

declare(strict_types=1);

namespace Drupal\cloud_native_image_styles;

use Drupal\cloud_native_image_styles\File\FileUrlGenerator;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Service provider to conditionally decorate the CDN file URL generator.
 */
class CloudNativeImageStylesServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritDoc}
   */
  public function alter(ContainerBuilder $container) {
    if ($container->has('cdn.file_url_generator')) {
      $container
        ->register('cloud_native_image_styles.file_url_generator', FileUrlGenerator::class)
        ->setDecoratedService('cdn.file_url_generator')
        ->addArgument(new Reference('cloud_native_image_styles.file_url_generator.inner'))
        ->addArgument(new Reference('cdn.settings'))
        ->addArgument(new Reference('entity_type.manager'));
    }
  }

}
