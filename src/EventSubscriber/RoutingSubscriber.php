<?php

declare(strict_types=1);

namespace Drupal\cloud_native_image_styles\EventSubscriber;

use Drupal\Core\Routing\RouteBuildEvent;
use Drupal\Core\Routing\RoutingEvents;
use Drupal\cloud_native_image_styles\Controller\ImageStyleDownloadController;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Routing subscriber to override the image style download controller.
 */
class RoutingSubscriber implements EventSubscriberInterface {

  /**
   * Alters the route.
   *
   * @param \Drupal\Core\Routing\RouteBuildEvent $event
   *   Route build event.
   */
  public function onAlter(RouteBuildEvent $event) {
    $collection = $event->getRouteCollection();
    $route = $collection->get('flysystem.image_style');
    if ($route) {
      $route->setDefault(
        '_controller',
        ImageStyleDownloadController::class . '::deliver'
      );
    }
  }

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents() {
    return [
      RoutingEvents::ALTER => ['onAlter'],
    ];
  }

}
