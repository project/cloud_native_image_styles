<?php

declare(strict_types=1);

namespace Drupal\cloud_native_image_styles\Entity;

use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\image\Entity\ImageStyle as UpstreamImageStyle;

/**
 * Extended implementation of ImageStyle to serve derivatives from a preferred
 * (the default) scheme instead of the source scheme.
 */
final class ImageStyle extends UpstreamImageStyle {

  /**
   * {@inheritDoc}
   */
  public function buildUri($uri) {
    $default_scheme = $this->fileDefaultScheme();
    // The upstream implementation uses the default scheme for non-URI paths.
    // @see https://www.drupal.org/project/drupal/issues/3218514
    $source_scheme = StreamWrapperManager::getScheme($uri) ?: $default_scheme;
    // Container parameter for custom mappings.
    $mapping = \Drupal::getContainer()
      ->getParameter('cloud_native_image_styles.mapping');
    if (array_key_exists($source_scheme, $mapping)) {
      $scheme = $mapping[$source_scheme];
      $path = StreamWrapperManager::getTarget($uri);
      return "$scheme://styles/{$this->id()}/$source_scheme/{$this->addExtension($path)}";
    }
    return parent::buildUri($uri);
  }

}
