<?php

declare(strict_types=1);

namespace Drupal\cloud_native_image_styles\Controller;

use Drupal\image\Controller\ImageStyleDownloadController as UpstreamController;
use Drupal\image\ImageStyleInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Wraps the image style controller for Flysystem stream wrappers.
 * Sets the Expires header similar to files delivered from disk.
 */
final class ImageStyleDownloadController extends UpstreamController {

  /**
   * Expires time; same as Drupal's default from the core .htaccess Apache
   * mod_expires implementation.
   */
  public const EXPIRES_TTL = 1209600;

  /**
   * {@inheritdoc}
   */
  public function deliver(Request $request, $scheme, ImageStyleInterface $image_style) {
    $result = parent::deliver($request, $scheme, $image_style);
    // hook_file_download_alter only called for private scheme in above.
    $result->headers->set(
      'Expires',
      gmdate(DATE_RFC1123, REQUEST_TIME + self::EXPIRES_TTL)
    );
    return $result;
  }

}
