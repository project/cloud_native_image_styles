<?php

declare(strict_types=1);

namespace Drupal\cloud_native_image_styles\File;

use Drupal\cdn\CdnSettings;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManager;

/**
 * Decorated file URL generator that creates the derivative when we first
 * generate the URL, instead of on first request from a client.
 */
class FileUrlGenerator {

  /**
   * The decorated File URL generator from CDN.
   *
   * @var object
   */
  protected $decoratedGenerator;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * CDN settings.
   *
   * @var \Drupal\cdn\CdnSettings
   */
  protected $settings;

  public function __construct(object $decoratedGenerator, CdnSettings $cdnSettings, EntityTypeManagerInterface $entityTypeManager) {
    $this->decoratedGenerator = $decoratedGenerator;
    $this->settings = $cdnSettings;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Magic method to proxy calls for non-overridden methods.
   */
  public function __call($method, $args) {
    return call_user_func_array([$this->decoratedGenerator, $method], $args);
  }

  /**
   * Decorates the CDN file URL generator.
   *
   * @see \Drupal\cdn\File\FileUrlGenerator
   *
   * @param string $uri
   *   The URI to a file for which we need a CDN URL, or the path to a shipped
   *   file.
   *
   * @return string|false
   *   A string containing the scheme-relative CDN file URI, or FALSE if this
   *   file URI should not be served from a CDN.
   */
  public function generate(string $uri) {
    if ($this->settings->farfutureIsEnabled()) {
      $scheme = StreamWrapperManager::getScheme($uri);
      $target = $scheme ? StreamWrapperManager::getTarget($uri) : NULL;
      if ($scheme && strpos($target, 'styles/') === 0 && !file_exists($uri)) {
        [, $imageStyleId, $scheme, $path] = explode('/', $target, 4);
        /** @var \Drupal\image\Entity\ImageStyle $imageStyle */
        $imageStyle = $this->entityTypeManager->getStorage('image_style')
          ->load($imageStyleId);
        $imageUri = "$scheme://$path";
        $imageStyle->createDerivative($imageUri, $imageStyle->buildUri($imageUri));
      }
    }
    return $this->decoratedGenerator->generate($uri);
  }

}
